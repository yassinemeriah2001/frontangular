import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface Attachment {
  title: string;
  status: string;
  url: string;
  image: string;
}
@Component({
  selector: 'app-concours',
  templateUrl: './concours.component.html',
  styleUrls: ['./concours.component.css']
})
export class ConcoursComponent implements OnInit {
  fileToUpload: File | null = null;
  attachments: Attachment[] = [
    { title: 'Concours 1', status: 'Open', url: 'assets/test.pdf', image: 'assets/test.pdf' },
    { title: 'Concours 2', status: 'Closed', url: 'assets/employee.png', image: 'assets/employee.png' },
    // Add more attachments as needed
  ];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }
  onFileInputChange(event: any) {
    this.fileToUpload = event.target.files[0];
  }
  uploadAttachment() {
    if (this.fileToUpload) {
      const formData = new FormData();
      formData.append('file', this.fileToUpload);
      // You can make an HTTP request to upload the file using this.formData
      // Example: this.http.post('https://example.com/upload', formData).subscribe(response => { ... });
    } else {
      // Handle error when no file is selected
    }
  }
  closeAttachment(attachment: Attachment): void {
    // Logic to close attachment
    attachment.status = 'Closed';
  }
  openAttachment(attachment: Attachment): void {
    // Logic to close attachment
    attachment.status = 'Open';
  }
}
