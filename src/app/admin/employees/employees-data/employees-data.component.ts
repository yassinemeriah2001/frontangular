import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employees-data',
  templateUrl: './employees-data.component.html',
  styleUrls: ['./employees-data.component.css']
})
export class EmployeesDataComponent implements OnInit {

  employees=[
    {"id":1,"name": "John Smith", "occupation": "Advisor", "age": 36,"photo":"assets/employee.png"},
    {"id":2,"name": "Muhi Masri", "occupation": "Developer", "age": 28,"photo":"assets/employee.png"},
    {"id":3,"name": "Peter Adams", "occupation": "HR", "age": 20,"photo":"assets/employee.png"},
    {"id":4,"name": "Lora Bay", "occupation": "Marketing", "age": 43,"photo":"assets/employee.png"},
    {"id":5,"name": "test test", "occupation": "HR", "age": 20,"photo":"assets/employee.png"},
]
  constructor() { }

  ngOnInit(): void {
  }

}
