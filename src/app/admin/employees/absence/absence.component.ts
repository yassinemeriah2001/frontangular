import { Component, OnInit } from '@angular/core';
declare var google: any;


@Component({
  selector: 'app-absence',
  templateUrl: './absence.component.html',
  styleUrls: ['./absence.component.css']
})
export class AbsenceComponent implements OnInit {

  constructor() { }
  ngOnInit(): void {
    
  }
  ngAfterViewInit(): void {
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(this.drawChart.bind(this));
  }

  drawChart() {
    const data = google.visualization.arrayToDataTable([
      ['Status', 'Number of Employees'],
      ['Absent', 40],
      ['Present', 90]
    ]);

    const options = {
      title: 'Employee Attendance Status',
      is3D: true,
      colors: ['#fdc613', '#081d5c'],
      width: 600,
      height: 600,
    };

    const chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }

}
