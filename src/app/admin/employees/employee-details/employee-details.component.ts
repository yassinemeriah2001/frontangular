import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare var google: any;
@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})

export class EmployeeDetailsComponent implements OnInit {


  employees=[
    {"id":1,"name": "John Smith", "occupation": "Advisor", "age": 36,"photo":"assets/employee.png","presenceH":4,"absenceH":2},
    {"id":2,"name": "Muhi Masri", "occupation": "Developer", "age": 28,"photo":"assets/employee.png","presenceH":15,"absenceH":8},
    {"id":3,"name": "Peter Adams", "occupation": "HR", "age": 20,"photo":"assets/employee.png","presenceH":6,"absenceH":1},
    {"id":4,"name": "Lora Bay", "occupation": "Marketing", "age": 43,"photo":"assets/employee.png","presenceH":8,"absenceH":3},
    {"id":5,"name": "test test", "occupation": "HR", "age": 20,"photo":"assets/employee.png","presenceH":9,"absenceH":15},
]
  currentemployee: { id: number; name: string; occupation: string; age: number; photo: string; } | any;
  
  constructor( private activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    for(let e of this.employees){
      if(this.activatedRoute.snapshot.params['id']==e.id){
        this.currentemployee=e;
      }
    }
  }
  ngAfterViewInit(): void {
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(this.drawChart.bind(this));
  }

  drawChart() {
    const data = google.visualization.arrayToDataTable([
      ['Status', 'Number of hours'],
      ['Absent', this.currentemployee.absenceH],
      ['Present', this.currentemployee.presenceH]
    ]);

    const options = {
      title: this.currentemployee.name+' Attendance Status',
      is3D: true,
      colors: ['#fdc613', '#081d5c'],
      width: 600,
      height: 600,
    };

    const chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }

}
