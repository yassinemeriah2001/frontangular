import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPageComponent } from './admin/admin-page/admin-page.component';
import { ConcoursComponent } from './admin/concours/concours.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AbsenceComponent } from './admin/employees/absence/absence.component';
import { EmployeeDetailsComponent } from './admin/employees/employee-details/employee-details.component';
import { EmployeesDataComponent } from './admin/employees/employees-data/employees-data.component';
import { SettingsComponent } from './admin/settings/settings.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './services/auth.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleChartsModule } from 'angular-google-charts';

const routes: Routes = [
  {path:'', redirectTo:'admin', pathMatch:'full'},
  {path:'login',component:LoginComponent},
  {path:'admin',component:AdminPageComponent,canActivate:[AuthGuard],
      children:[{path:'dashboard',component:DashboardComponent},
      {path:'concours',component:ConcoursComponent},
      {path:'employeesData',component:EmployeesDataComponent},
      {path:'employeesData/:id',component:EmployeeDetailsComponent,pathMatch:'full'},
      {path:'employeesAbsence',component:AbsenceComponent,pathMatch:'full'},
      {path:'Settings',component:SettingsComponent,pathMatch:'full'}]},
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    GoogleChartsModule, ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
