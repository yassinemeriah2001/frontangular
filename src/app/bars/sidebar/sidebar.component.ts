import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  showing:boolean=true
  constructor() { }

  ngOnInit(): void {
  }
show=():void=>{
  this.showing=!this.showing
  console.log(this.showing)
}
}
