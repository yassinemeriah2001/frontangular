import { Component, OnInit } from '@angular/core';
import { AccountDataService } from '../../services/account-data.service';
import { AuthGuard } from '../../services/auth.guard';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  hidden:boolean=false
  hiddenl:boolean=true
  constructor(private authService:AccountDataService, private authguard:AuthGuard) { 
    
  }

  ngOnInit(): void {
    if(this.authguard.access==true){
      this.hidden=true
      this.hiddenl=false
    }
    else{
      this.hidden=false
      this.hiddenl=true
    }
  }

}
