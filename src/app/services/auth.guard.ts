import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AccountDataService } from './account-data.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  access:boolean=false
  constructor(private authService:AccountDataService, private router:Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.access
    
  }
  
}
