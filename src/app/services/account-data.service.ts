import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminAccount } from 'src/models/AdminAccount';

@Injectable({
  providedIn: 'root',
})
export class AccountDataService {
  currentAccount: AdminAccount | null = null;
  showLinks: boolean = false;
  constructor(private http: HttpClient) {}

  getCurrentAccount(){  
    return this.currentAccount
  }

  getAccount(identifiant: string, motDePasse: string):Observable<HttpResponse<AdminAccount>> {
    return this.http
      .post<AdminAccount>('http://localhost:3000/accountsApi/admin/auth', {
        identifiant,
        motDePasse,
      },{observe:"response"})
      
  }
}
