import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './bars/navbar/navbar.component';
import { AdminPageComponent } from './admin/admin-page/admin-page.component';
import { AccountDataService } from './services/account-data.service';
import { AuthGuard } from './services/auth.guard';
import { HttpClientModule } from '@angular/common/http';
import { SidebarComponent } from './bars/sidebar/sidebar.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { ConcoursComponent } from './admin/concours/concours.component';
import { EmployeesDataComponent } from './admin/employees/employees-data/employees-data.component';
import { EmployeeDetailsComponent } from './admin/employees/employee-details/employee-details.component';
import { AbsenceComponent } from './admin/employees/absence/absence.component';
import { SettingsComponent } from './admin/settings/settings.component'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    AdminPageComponent,
    SidebarComponent,
    DashboardComponent,
    ConcoursComponent,
    EmployeesDataComponent,
    EmployeeDetailsComponent,
    AbsenceComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [AccountDataService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
