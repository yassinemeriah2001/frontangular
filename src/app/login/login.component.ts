import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { AccountDataService } from '../services/account-data.service';
import { AuthGuard } from '../services/auth.guard';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hidden=true
  admin = {
    email: null,
    password: null
  };
  constructor(private router:Router, private authService:AccountDataService, private authguard:AuthGuard) { }

  ngOnInit(): void {
    
  }
  onSubmit(admin: any) {
    this.authService.getAccount(admin.email,admin.password).subscribe((response) => {
      console.log(response.status)
      this.authguard.access=true
      this.router.navigate(['/admin/dashboard']);
      
    },(err)=>{if (err.status==401){
      this.hidden=false
    }});;
    
    
  }
  
}
